provider "aws" {
  region  = "ap-south-1"
  version = "~> 2.41"
}


resource "aws_instance" "ec2" {

  ami = "ami-04b1ddd35fd71475a"

  instance_type               = var.ec2_instance.instance_type
  iam_instance_profile        = var.ec2_iam_instance_role
  vpc_security_group_ids      = var.security_group_ids
  subnet_id                   = var.private_subnet_id
  associate_public_ip_address = true
  key_name                    = "terraform-practice1"


  provisioner "file" {
    source      = "./surya.txt"
    destination = "/tmp/surya.txt"

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("terraform-practice1.pem")
      host        = self.public_ip
    }
  }
}

resource "aws_s3_bucket" "b" {
  bucket = var.s3_bucket_name
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


